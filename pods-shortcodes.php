<?php
/**
 * Plugin Name: PODS Shortcodes
 * Description: Add commonly used shortcodes
 */

namespace PODS\Shortcodes;

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


if (!class_exists('Shortcodes')) {
    class Shortcodes
    {

        // vars
        var $settings;


        /*
        *  __construct
        *
        *  A dummy constructor to ensure initialized once
        */

        function __construct() {

            /* Do nothing here */

        }


        /*
        *  initialize
        *
        *  The real constructor to initialize
        *
        */

        public function initialize()
        {

            // vars
            $this->settings = array(
                // basic
                'name'              => __('PODS Shortcodes'),
                'version'           => '0.1',

                // urls
                'basename'          => plugin_basename( __FILE__ ),
                'path'              => plugin_dir_path( __FILE__ ),
                'dir'               => plugin_dir_url( __FILE__ ),
            );

            // shortcodes
            add_shortcode( 'phpinclude', array( $this, 'shortcode_php_include' ) );

        }


        /**
         * PHP include file
         *
         * @since  1.0.0
         * @param  array  $params
         * @return void
         */
        public function shortcode_php_include ( $atts = array() ) {
           //if file was specified
           extract(
                shortcode_atts(
                     array(
                          'file' => 'NULL'
                     ), $atts
                )
           );

           //BEGIN modified portion of code to accept query strings
           //check for query string of variables after file path
           if(strpos($file,"?")) {
                $query_string_pos = strpos($file,"?");
                //create global variable for query string so we can access it in our included files if we need it
                //also parse it out from the clean file name which we will store in a new variable for including
                global $query_string;
                $query_string = substr($file,$query_string_pos + 1);
                $clean_file_path = substr($file,0,$query_string_pos);
                //if there isn't a query string
           } else {
                $clean_file_path = $file;
           }
           //END modified portion of code

           $filepath = get_theme_root() .'/'. get_template() .'/'. $clean_file_path;

           //check if the file was specified and if the file exists
           if ($file != 'NULL' && file_exists($filepath)){
                //turn on output buffering to capture script output
                ob_start();
                //include the specified file
                include($filepath);

                //assign the file output to $content variable and clean buffer
                $content = ob_get_clean();
                //return the $content
                //return is important for the output to appear at the correct position
                //in the content
                return $content;
           }
        }
    }
}


/**
 * Instantiates the Shortcodes
 */
function runShortcodes()
{
    $run = new Shortcodes();
    $run->initialize();
}

runShortcodes();
